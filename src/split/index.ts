import { Interceptor, Test } from '../../index';

import SplitTest from './split-test';

export default class Split {
    public tests: SplitTest[] = [];

    public setDefaultUser(user: string) {
        SplitTest.defaultUser = user;
    }

	public addInterceptor(interceptor: Interceptor) {
		SplitTest.interceptors.push(interceptor);
	}

    public add(test: Test): Split {
        const existTest = this.get(test.name);

        if (!existTest)
            this.tests.push(new SplitTest(test));

        return this;
    }

    public get(name: string): SplitTest|undefined {
        return this.tests.find((t: Test) => t.name === name);
    }

    public run(name: string): SplitTest {
        const test = this.get(name);

        if (!test)
            throw Error(`[Split]: test ${name} not found`);

        return test.run();
    }

    public addAndRun(test: Test): SplitTest {
        const existTest = this.get(test.name);

        if (existTest) {
            console.log(`[Split]: ${existTest.name} already exist`)
            return existTest.run();
        } else {
            const splitTest = new SplitTest(test).run();
            this.tests.push(splitTest);
            return splitTest;
        }
    }
}
