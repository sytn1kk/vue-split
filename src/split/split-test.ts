import { Test, Interceptor, Group } from '../../index'

import SplitValidator from './split-validator'
import SplitExecutor from './split-executor'

export default class SplitTest {
	public static defaultUser: string;
	public static interceptors: Interceptor[] = [];

	public name: string;
	public user: string;
	public groups: Group[];
	public result?: Group['name'];
	public callback?: (test: SplitTest) => void;

	constructor({ name, user, groups, callback }: Test) {
		this.name = name;
		this.user = user ? user : SplitTest.defaultUser;
		this.groups = groups;
		this.callback = callback;

		this.validate();
	}

	get resultGroup(): Group|undefined {
		if (this.result)
			return this.groups
				.find(g => g.name === this.result)
	}

	public validate(): void {
		new SplitValidator(this);
	}

	public run(): SplitTest {
		new SplitExecutor(this);
		return this;
	}
}