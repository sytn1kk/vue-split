import md5 from 'blueimp-md5';

import { Group } from "../../index";
import SplitTest from './split-test';

export default class SplitExecutor {
	private defaultWeight: number = 0.5;

	private test: SplitTest;

	constructor(test: SplitTest) {
		this.test = test;

		const groups = this.prepareGroups();
        const random = this.getRandom();

		const [ filtered ] = groups.filter((g: Group) => g._w > random);

        if (!filtered)
            throw Error('[Split] Error filtering the groups.');

		this.test.result = filtered.name;

        SplitTest.interceptors.forEach((interceptor) => {
            interceptor(test);
        });

		if (this.test.callback)
			this.test.callback(test);
	}

	private getTotalWeight(): number {
        return this.test.groups
            .map((g: Group) => isFinite(g.weight) ? Number(g.weight) : this.defaultWeight)
            .reduce((a, b) => a + b)
    }

	private getRandom(): number {
        const hexMD5hash = md5(this.test.name, this.test.user).substr(0,8);
        const hashAsInt = parseInt("0x" + hexMD5hash, 16);
        const maxInt = parseInt("0xffffffff", 16);
        return hashAsInt / maxInt;
    }

	private prepareGroups(): Group[] {
        const totalWeight = this.getTotalWeight();

        let accumulativeWeight = 0;

        return this.test.groups
            .map((g: Group) => ({ 
                ...g,
                _w: (isFinite(g.weight) ? Number(g.weight) : this.defaultWeight) / totalWeight
            }))
            .sort((a, b) => b._w - a._w)
            .map((g: Group) => {
                g._w += accumulativeWeight;
                accumulativeWeight = g._w;
                return g;
            })
    }
}
