import SplitTest from './split-test';

import { Group } from "../../index";

export default class SplitValidator {
  constructor(test: SplitTest) {
    if (test.name === undefined || typeof test.name !== 'string')
        throw Error('[Split] Undefined name. Name must be a string.');

    if (test.user === undefined || typeof test.user !== 'string')
        throw Error('[Split] Undefined user. User must be a string.');

	if (!Array.isArray(test.groups) || test.groups.length === 0)
        throw Error(`[Split] Test groups is emplty. Check ${test.name} configuration.`);

	test.groups.forEach((g: Group) => {
		if (!g.name || !g.weight)
            throw Error(`[Split] Test group incorrect. Check ${test.name} configuration.`);
	});

    if (test.callback && typeof test.callback !== 'function')
        throw Error(`[Split] Callback should be a function. Check ${test.name} configuration.`);

    SplitTest.interceptors.forEach((interceptor) => {
        if (typeof interceptor !== 'function')
            throw Error(`[Split] Interceptor should be a function. Check ${test.name} configuration.`);
    })
  }
}