export default {
    name: 'split',
    props: {
        name: {
            type: String,
            required: true,
        },
        user: {
            type: String,
        },
        callback: {
            type: Function,
        }
    },
    methods: {
        filterSplitGroupNodes() {
            return this.$slots.default.filter(({ componentOptions }) => {
                return componentOptions?.propsData?.weight
                    && componentOptions?.propsData?.name;
            })
        },
        runTest(splitGroupNodes) {
            const test = this.$split.get(this.name);

            if (!test) {
                this.$split.addAndRun({
                    name: this.name,
                    user: this.user,
                    callback: (test) => this.$emit('callback', test),
                    groups: splitGroupNodes.map((splitGroupVNode) => {
                        return splitGroupVNode.componentOptions.propsData;
                    })
                })
            }
        },
        getSplitGroupNodeByTestResult(splitGroupNodes) {
            return splitGroupNodes.filter((testGroupVNode) => {
                const { name } = testGroupVNode.componentOptions.propsData;
                return this.$split.get(this.name).result === name;
            })
        }
    },
    render() {
        const splitGroupNodes = this.filterSplitGroupNodes();
        this.runTest(splitGroupNodes);
        return this.getSplitGroupNodeByTestResult(splitGroupNodes);
    }
}
