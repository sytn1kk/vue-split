export default {
	name: 'split-group',
	props: {
        name: {
            type: String,
            required: true,
        },
        weight: {
            type: String,
            required: true,
        },
    },
	render() {
        return this.$slots.default;
    }
}