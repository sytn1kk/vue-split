import { Vue as _Vue, VueConstructor } from 'vue/types/vue'

import { SplitPluginOptions } from '../index';
import Split from './split';

import SplitComponent from './components/split'
import SplitGroupComponent from './components/split-group'

export { default as Split } from './split'

export default class SplitPlugin {
  public static instance: Split;

  public static install(Vue: VueConstructor<Vue>, options: SplitPluginOptions = { rootComponentName: 'split', groupComponentName: 'split-group' }) {
    this.instance = new Split();
    Vue.prototype.$split = this.instance;
    Vue.component(options.rootComponentName, SplitComponent);
    Vue.component(options.groupComponentName, SplitGroupComponent);
  }
}