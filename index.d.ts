import { PluginFunction } from 'vue'

import Split from './src/split'
import SplitTest from './src/split/split-test'

declare module 'vue/types/vue' {
    interface Vue {
        $split: Split;
    }
}

export declare type SplitPluginOptions = {
    rootComponentName: string;
    groupComponentName: string;
}

export declare type Group = {
    name: string;
    weight: number;
    [key: string]: any;
}

export declare type Test = {
    name: string;
    user: string;
    groups: Group[];
    result?: Group['name'];
    callback?: (test: SplitTest) => void;
}

declare class SplitPlugin {
    static instance: Split;
    static install: PluginFunction<SplitPluginOptions>;
}

export declare type Interceptor = (test: Test) => void

export default SplitPlugin;