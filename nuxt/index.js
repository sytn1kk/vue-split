const { resolve } = require('path')

module.exports = function nuxtVueSplitTesting(moduleOptions) {
    this.nuxt.hook('build:before', () => {
        this.addPlugin({
            src: resolve(__dirname, 'plugin.template.js'),
            fileName: 'vue-split-testing.js',
            options: moduleOptions
        })
    })
}

module.exports.meta = require('../package.json')