import Vue from 'vue';
import VueSplit from 'vue-split-testing';

Vue.use(VueSplit);

export default (ctx, inject) => {
  const { instance } = VueSplit;
  ctx.$split = instance;
  inject('split', instance);
}