import * as amplitude from 'amplitude-js'

import { uuidv4 } from '~/utils'

export default ({ $config, store }, inject) => {
    const user_token = uuidv4()
    const amplitudeInstance = amplitude.getInstance()

    store.commit('SET_USER_TOKEN', user_token)

    amplitudeInstance.init($config.amplitudeApiKey)

    amplitudeInstance.setUserId(user_token)
    amplitudeInstance.setUserProperties({
        environment: $config.NODE_ENV,
    })

    inject('amplitude', amplitudeInstance)
}
