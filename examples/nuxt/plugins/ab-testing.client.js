import Vue from 'vue'
import VueSplit from '../../../dist/index'

Vue.use(VueSplit)

export default ({ $amplitude, store }, inject) => {
    const $split = VueSplit.instance

    $split.addInterceptor((test) => {
        console.log(test)
        $amplitude.setUserProperties({
            [test.name]: test.result
        })
    })

    $split.setDefaultUser(store.state.user_token)
    
    inject('split', $split)
}