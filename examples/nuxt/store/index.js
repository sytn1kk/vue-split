export const state = () => ({
    user_token: ''
})

export const mutations = {
    SET_USER_TOKEN(state, user_token) {
        state.user_token = user_token
    },
}