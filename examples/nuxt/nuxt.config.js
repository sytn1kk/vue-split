require('dotenv').config()

export default {
    plugins: [
        '~/plugins/amplitude.client.js',
        '~/plugins/ab-testing.client.js',
    ],
    publicRuntimeConfig: {
        amplitudeApiKey: process.env.AMPLITUDE_API_KEY,
        NODE_ENV: process.env.NODE_ENV,
    }
}